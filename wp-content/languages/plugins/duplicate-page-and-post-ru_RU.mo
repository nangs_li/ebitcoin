��          \      �       �   !   �   	   �      �   -     '   ;  6   c     �  z  �  @   "     c  5   z  N   �  X   �  6   X     �                                       Are you sure you want to do this? Duplicate Duplicate Page And Post Duplicate a Post or Page with just one click. No post to duplicate has been supplied! https://wordpress.org/plugins/duplicate-page-and-post/ pluginsforwp PO-Revision-Date: 2017-09-28 15:58:06+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: GlotPress/2.4.0-alpha
Language: ru
Project-Id-Version: Plugins - Duplicate Page and Post - Stable (latest release)
 Вы уверенны что хотите сделать это? Дублировать Дублируйте страницы и записи Дублируйте страницы и записи одним кликом. Ни одна запись не была выбрана для дублирования! https://wordpress.org/plugins/duplicate-page-and-post/ pluginsforwp 